<?php
session_start();

// контролер
Class Controller_Index Extends Controller_Base
{

    // шаблон
    public $layouts = "first_layouts";

    // экшен
    function index()
    {
        if (isset($_GET['page']))
            $page = $_GET['page'];
        if (isset($_GET['sort']))
            $sort = $_GET['sort'];
        if (isset($_GET['order']))
            $order = $_GET['order'];

        $model = new Model_Tasks();

        $taskList = $model->getAllTasks(empty($page) ? 1 : $page, empty($sort) ? 'id' : $sort, empty($order) ? 'DESC' : $order);

        $this->template->vars('allTasks', $taskList);
        $this->template->view('index');
    }

    function task()
    {

        $success = "Новая задача успешно добавлена";
        if (isset($_POST['add'])) {
            $tasks = new Model_Tasks();
            $name = $_POST['name'];
            $email = $_POST['email'];
            $task = $_POST['task'];

            if (!empty($tasks->addTask($email, $name, $task))) {
                $this->template->vars('success', $success);
            }
        }

        $this->template->view('task');
    }

    function task_admin()
    {
        if (isset($_SESSION['user'])) {
            if (isset($_GET['page']))
                $page = $_GET['page'];
            if (isset($_GET['sort']))
                $sort = $_GET['sort'];
            if (isset($_GET['order']))
                $order = $_GET['order'];
            $model = new Model_Tasks();

            $taskList = $model->getAllTasks(empty($page) ? 1 : $page, empty($sort) ? 'id' : $sort, empty($order) ? 'DESC' : $order, 1);

            if (isset($_POST['update'])) {
                $name = $_POST['name'];
                $email = $_POST['email'];
                $task = $_POST['task'];
                $che = $_POST['checked'];
                $id = $_GET['edit'];
                if (empty($che))
                    $che = 0;
                $model->updateTask($email, $name, $task, $che, $id);
                ob_start();
                echo 'output';
                header("Location: /task_admin");
                ob_end_flush();
            }
            $this->template->vars('allTasks', $taskList);
            if (isset($_GET['edit'])) {
                $id = $_GET['edit'];
                $task = $model->getTaskById($id);
                $this->template->vars('task', $task);
                $this->template->view('task_edit_admin');
            } else
                $this->template->view('task_admin');
        } else {

            $this->logout();
        }
    }

    // Выход из системы
    function logout()
    {
        $login = new Model_Login();
        $login->logout();
        $login->redirect('login');
    }

    // Перенаправления
    function redirect($url)
    {
        $login = new Model_Login();
        $login->redirect($url);
    }

}