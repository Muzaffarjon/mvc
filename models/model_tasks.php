<?php
include("classes/DBController.php");

// модель
Class Model_Tasks
{

    private $db_handle;

    private $date;

    /**
     * Model_Tasks constructor.
     */
    function __construct()
    {
        $this->db_handle = new DBController();
        $this->date = time();
    }

    /**
     * @param $email
     * @param $name
     * @param $task
     * @return int
     */
    function addTask($email, $name, $task)
    {
        $checked = 0;

        $query = "INSERT INTO task (name,email,task,checked,datecreate,lastupdate) VALUES (?, ?, ?,?,?,?)";
        $paramType = "ssssss";
        $paramValue = array(
            $name,
            $email,
            $task,
            $checked,
            $this->date,
            $this->date
        );

        $insertId = $this->db_handle->insert($query, $paramType, $paramValue);
        return $insertId;
    }

    /**
     * @param $email
     * @param $name
     * @param $task
     * @param $checked
     * @param $task_id
     * @return bool
     */
    function updateTask($email, $name, $task, $checked, $task_id)
    {
        // обновляем запись
        $query = "UPDATE task SET name = ?, email = ?,task = ?,checked = ?,lastupdate = ? WHERE id = ?";
        $paramType = "ssssss";
        $paramValue = array(
            $name,
            $email,
            $task,
            $checked,
            $this->date,
            $task_id
        );

        if ($this->db_handle->update($query, $paramType, $paramValue))
            return true;

        return false;
    }


    /**
     * @param $task_id
     * @return array
     */
    function getTaskById($task_id)
    {
        //выборка по id записи
        $query = "SELECT * FROM task WHERE id = ?";
        $paramType = "i";
        $paramValue = array(
            $task_id
        );

        $result = $this->db_handle->runQuery($query, $paramType, $paramValue);
        return $result;
    }

    /**
     * @param $page
     * @param $sort
     * @param null $order
     * @param null $isAdmin
     * @return array
     */
    function getAllTasks($page, $sort, $order = null, $isAdmin = null)
    {
        $sql = "SELECT * FROM task ORDER BY id";
        $result_count = $this->db_handle->runBaseQuery($sql);
        // Пагинация записей
        $page = (isset($page) && $page < 100000) ? (int)$page : 1;
        if (!is_null($isAdmin))
            $perPage = 10;
        else
            $perPage = 3;
        $start = $perPage * ($page - 1);
        $total = count($result_count);
        $totalPages = ceil($total / $perPage);

        $next = $page + 1;
        $prev = $page - 1;
        $sql = "SELECT * FROM task ORDER BY $sort $order LIMIT $start, $perPage";
        $result = $this->db_handle->runBaseQuery($sql);

        $paginatoinInfo = [
            "page" => $page,
            "start" => $start,
            "totalPages" => $totalPages,
            "next" => $next,
            "prev" => $prev
        ];

        $res = [];
        $res['tasks'] = $result;
        $res['paginator'] = $paginatoinInfo;

        return $res;
    }

}