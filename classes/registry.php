<?php
// Класс хранилища
Class Registry {

	private $vars = array();
	
	// запись данных

    /**
     * @param $key
     * @param $var
     * @return bool
     * @throws Exception
     */
    function set($key, $var) {
        if (isset($this->vars[$key]) == true) {
			throw new Exception('Unable to set var `' . $key . '`. Already set.');
        }
        $this->vars[$key] = $var;
        return true;
	}

	// получение данных

    /**
     * @param $key
     * @return mixed|null
     */
    function get($key) {
		if (isset($this->vars[$key]) == false) {
			return null;
		}
		return $this->vars[$key];
	}

	// удаление данных

    /**
     * @param $var
     */
    function remove($var) {
		unset($this->vars[$key]);
	}
}