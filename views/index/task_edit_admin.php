<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script>
    function validate() {
        var valid = true;
        $(".demoInputBox").css('background-color','');
        $(".demoInputBox").css('border-color','');
        $(".sinfo").html('');

        if(!$("#email").val()) {
            $("#email-error").html("(required)");
            $("#email").css('background-color','#FFFFDF');
            $("#email").css('border-color','red');
            valid = false;
        }
        if(!$("#name").val()) {
            $("#name-error").html("(required)");
            $("#name").css('background-color','#FFFFDF');
            $("#name").css('border-color','red');
            valid = false;
        }
        if(!$("#task").val()) {
            $("#task-error").html("(required)");
            $("#task").css('background-color','#FFFFDF');
            $("#task").css('border-color','red');
            valid = false;
        }
        return valid;
    }
</script>

<form method="POST" name="myform" action="" onSubmit="return validate();">
    <div class="form-group">
        <label for="exampleInputEmail1">Email </label> <span id="email-error" class="text-muted"></span>
        <input type="email" id="email" class="form-control " value="<?= $task[0]['email']; ?>" name="email" placeholder="Enter email">

    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Имя</label> <span id="name-error" style="color: red" class="text-muted"></span>
        <input type="text" id="name" name="name" value="<?= $task[0]['name']; ?>" class="form-control demoInputBox" placeholder="Имя">
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Текст задачи</label> <span id="task-error" style="color: red" class="text-muted"></span>
        <textarea type="text" id="task" name="task" class="form-control demoInputBox" placeholder="Текст задачи"><?= $task[0]['task']; ?></textarea>
    </div>
    <div class="custom-control custom-checkbox">
        <input type="checkbox" name="checked" value="1" class="custom-control-input" <?php if($task[0]['checked']==1) echo 'checked'; ?> id="defaultUnchecked">
        <label class="custom-control-label" for="defaultUnchecked">Задание выполнено</label>
    </div>
    <button type="submit" name="update" class="btn btn-primary">Обновить задачу</button>
</form>
