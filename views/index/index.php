<?php
if(isset($_GET['order'])){
    $order=$_GET['order'];
    if($order=='DESC')
        $order='ASC';
    else
        $order='DESC';
}else{
    $order='DESC';
}
?>
<table class="table">

    <thead class="thead-dark">
    <tr>
        <th scope="col">#</th>
        <th scope="col"><a
                    href="<?php if (isset($_GET['page'])) echo '?page=' . $_GET['page'] . '&sort=name&order='.$order; else echo '?sort=name&order='.$order; ?>">Имя</a>
        </th>
        <th scope="col"><a
                    href="<?php if (isset($_GET['page'])) echo '?page=' . $_GET['page'] . '&sort=email&order='.$order; else echo '?sort=email&order='.$order; ?>">Email</a>
        </th>
        <th scope="col">Задача</th>
        <th scope="col"><a
                    href="<?php if (isset($_GET['page'])) echo '?page=' . $_GET['page'] . '&sort=checked&order='.$order; else echo '?sort=checked&order='.$order; ?>">Статус</a>
        </th>
    </tr>
    </thead>
    <tbody>
    <?php
    if (isset($allTasks)) {
        $i = 1;
        foreach ($allTasks['tasks'] as $all) {
            ?>
            <tr>
                <th scope="row"><?= $all['id']; ?></th>
                <td><?= $all['name']; ?></td>
                <td><?= $all['email']; ?></td>
                <td><?= htmlspecialchars($all['task']); ?></td>
                <td><?= ($all['checked'] == 1) ? 'выполнено' : 'в ожидании'; ?></td>
            </tr>
            <?php
        }
    }
    ?>

    </tbody>
</table>

<nav aria-label="Page navigation example">
    <ul class="pagination">
        <?php
        for ($i = 1; $i <= $allTasks['paginator']['totalPages']; $i++) {
        if (count($allTasks['tasks']) >= 3) {
            $url="";
            ?>
                <li class="page-item">
                    <a class="page-link"
                                         href="<?php
                                         $url='?page='.$i;
                                         $url.=isset($_GET['sort']) ? '&sort='.$_GET['sort'] : '';
                                         $url.=isset($_GET['order']) ? '&order='.$_GET['order'] : '';
                                         echo $url;
                                         ?>">
                                        <?= $i; ?>
                    </a>
                </li>
                <?php
        }
        }
        ?>
    </ul>
</nav>