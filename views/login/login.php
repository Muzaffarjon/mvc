
<form method="post" action="" class="form-signin">
    <div class="text-center mb-4">
        <?= isset($errorMessage) ? '<div class="alert alert-danger" role="alert">
   '.$errorMessage.'
</div>' : ''; ?>
        <h1 class="h3 mb-3 font-weight-normal">Вход</h1>
    </div>

    <div class="form-label-group">
        <input type="text" id="inputEmail" class="form-control" name="login" placeholder="Login" autofocus="">
        <label for="inputEmail">Login</label>
    </div>

    <div class="form-label-group">
        <input type="password" id="inputPassword" class="form-control" name="password" placeholder="Password" >
        <label for="inputPassword">Password</label>
    </div>


    <button type="submit" name="ok" class="btn btn-lg btn-primary btn-block" type="submit">Вход</button>
    <p class="mt-5 mb-3 text-muted text-center">    <a href="/index">На главную</a>
    </p>
</form>