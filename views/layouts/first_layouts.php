<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="web/css/bootstrap.min.css">
    <link rel="stylesheet" href="web/css/floating-labels.css">

    <title>Тестовое задание</title>
</head>
<body>


<nav class="navbar navbar-expand-md fixed-top">
    <a class="navbar-brand" href="#">Тестовое задание</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">

            <?php
            if(isset($_SESSION['user'])){
            ?>
            <li class="nav-item">
                <a class="nav-link" href="/task_admin">Список задач</a>
            </li>
                <li class="nav-item">
                    <a class="nav-link" href="/logout">Выход (<?= $_SESSION['user'][0]['name']?>)</a>
                </li>
            <?php }else{ ?>
                <li class="nav-item">
                    <a class="nav-link" href="/index">Главная</a>
                </li>
            <li class="nav-item">
                <a class="nav-link" href="/login">Вход</a>
            </li>

            <?php } ?>
            <li class="nav-item">
                <a class="nav-link" href="/task">Создать задачу</a>
            </li>
        </ul>
    </div>
</nav>

<main role="main" class="container">

    <?php
    include ($contentPage);
    ?>

</main><!-- /.container -->
</body>
<body>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
</html>